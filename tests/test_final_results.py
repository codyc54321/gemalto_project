import unittest

from gemalto_project.bytes_to_data import parse_bytes_to_python_data_type


class FinalResultsTests(unittest.TestCase):

    def test_result_1(self):
        result = parse_bytes_to_python_data_type(1)
        self.assertEqual(result, 1)

    def test_result_2(self):
        result = parse_bytes_to_python_data_type(2)
        self.assertEqual(result, [1])

    def test_result_3(self):
        result = parse_bytes_to_python_data_type(3)
        expectation = {
            'a': 'b',
            1: '0000'
        }
        self.assertEqual(result, expectation)

    def test_result_4(self):
        result = parse_bytes_to_python_data_type(4)
        expectation = {
            'first': 16777216,
            'second': 'value for second',
            'deep': {
                1: 'integer as key',
                'mix': 'it is possible to mix integers and strings',
                2: {
                    4: 19088743
                }
            },
            5: 25
        }
        self.assertEqual(result, expectation)

    def test_result_5(self):
        result = parse_bytes_to_python_data_type(5)
        expectation = {
            'firstName': 'John',
            'lastName': 'Smith',
            'age': 25,
            'address': {
                'streetAddress': '21 2nd Street',
                'city': 'New York',
                'state': 'NY',
                'postalCode': '10021'
            },
            'phoneNumber': [
                {
                    'type': 'home',
                    'number': '212 555-1234'
                },
                {
                    'type': 'fax',
                    'number': '646 555-4567'
                }
            ]
        }
        self.assertEqual(result, expectation)

import unittest

from gemalto_project.bytes_to_data import (
    gather_bytes_array_from_file,
    preparse_bytes,
    determine_blank_value,
    retrieve_blank_order_value,
    preparse_dicts,
    remove_key_from_dicts,
    fill_lists,
    fill_blank_items_into_dicts,
    parse_bytes_to_python_data_type,

)

class JSONByteParserTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.bytes_1 = gather_bytes_array_from_file(1)
        cls.bytes_2 = gather_bytes_array_from_file(2)
        cls.bytes_5 = gather_bytes_array_from_file(5)
        cls.preparsed_array_2 = preparse_bytes(cls.bytes_2)
        cls.preparsed_array_5 = preparse_bytes(cls.bytes_5)
        cls.preparsed_dicts_5 = preparse_dicts(cls.preparsed_array_5)
        cls.input_5_expected_result = {
            'firstName': 'John', 'lastName': 'Smith', 'age': 25,
            'phoneNumber': [
                {'type': 'home', 'number': '212 555-1234'},
                {'type': 'fax', 'number': '646 555-4567'}],
            'address': {
                'city': 'New York', 'streetAddress': '21 2nd Street',
                'postalCode': '10021', 'state': 'NY'
            }
        }

    def test_gather_bytes_array_from_file(self):
        self.assertEqual(self.bytes_1, [1, 0, 0, 0, 1])


    def test_preparse_bytes(self):
        self.assertEqual(self.preparsed_array_2, [5, 1, 1])

    def test_determine_blank_value_no_keys(self):
        result = determine_blank_value({'blah': 'no keys'})
        self.assertEqual(result, 'xx_blank_1')

    def test_determine_blank_value_keys_exist(self):
        result = determine_blank_value({'name': 'xx_blank_1', 'age': 'xx_blank_4'})
        self.assertEqual(result, 'xx_blank_5')

    def test_retrieve_blank_order_value_none(self):
        result = retrieve_blank_order_value('no blank value')
        self.assertIsNone(result)

    def test_retrieve_blank_order_value_found(self):
        result = retrieve_blank_order_value('xx_blank_4')
        self.assertEqual(result, 4)

    def test_preparse_dicts(self):
        expectation = [
            {
                'firstName': 'John', 'lastName': 'Smith', 'age': 25, 'xxxkeyCountxxx': 5,
                'phoneNumber': 'xx_blank_2', 'address': 'xx_blank_1'
            },
            {
                'xxxkeyCountxxx': 4, 'city': 'New York', 'streetAddress': '21 2nd Street',
                'postalCode': '10021', 'state': 'NY'
            },
            5, 2,
            {'xxxkeyCountxxx': 2, 'type': 'home', 'number': '212 555-1234'},
            {'xxxkeyCountxxx': 2, 'type': 'fax', 'number': '646 555-4567'}
        ]
        self.assertEqual(self.preparsed_dicts_5, expectation)

    def test_remove_key_from_items(self):
        items = [
            {
                'firstName': 'John', 'lastName': 'Smith', 'age': 25, 'phoneNumber': 'xx_blank_2',
                'address': 'xx_blank_1', 'xxxkeyCountxxx': 5
            },
            {   'xxxkeyCountxxx': 4, 'city': 'New York', 'streetAddress': '21 2nd Street',
                'postalCode': '10021', 'state': 'NY'
                },
            [
                {'xxxkeyCountxxx': 2, 'type': 'home', 'number': '212 555-1234'},
                {'xxxkeyCountxxx': 2, 'type': 'fax', 'number': '646 555-4567'}
            ]
        ]
        expectation = [
            {
                'firstName': 'John', 'lastName': 'Smith', 'age': 25, 'phoneNumber': 'xx_blank_2',
                'address': 'xx_blank_1'
            },
            {   'city': 'New York', 'streetAddress': '21 2nd Street',
                'postalCode': '10021', 'state': 'NY'
                },
            [
                {'type': 'home', 'number': '212 555-1234'},
                {'type': 'fax', 'number': '646 555-4567'}
            ]
        ]
        result = remove_key_from_dicts(items, 'xxxkeyCountxxx')

    def test_fill_lists(self):
        # using in setUpClass causes a strange error
        bytes = gather_bytes_array_from_file(5)
        preparsed_bytes = preparse_bytes(bytes)
        preparsed_dicts = preparse_dicts(preparsed_bytes)
        result = fill_lists(preparsed_dicts)
        expectation = [
            {
                'firstName': 'John', 'lastName': 'Smith', 'age': 25, 'phoneNumber': 'xx_blank_2',
                'address': 'xx_blank_1'
            },
            {'city': 'New York', 'streetAddress': '21 2nd Street', 'postalCode': '10021', 'state': 'NY'},
            [{'type': 'home', 'number': '212 555-1234'}, {'type': 'fax', 'number': '646 555-4567'}]
        ]
        self.assertEqual(result, expectation)

    def test_fill_blank_items_into_dicts(self):
        bytes = gather_bytes_array_from_file(5)
        preparsed_bytes = preparse_bytes(bytes)
        preparsed_dicts = preparse_dicts(preparsed_bytes)
        parsed_lists = fill_lists(preparsed_dicts)
        result = fill_blank_items_into_dicts(parsed_lists)
        self.assertEqual(result, self.input_5_expected_result)

    def test_parse_bytes_to_python_data_type(self):
        result = parse_bytes_to_python_data_type(5)
        self.assertEqual(result, self.input_5_expected_result)

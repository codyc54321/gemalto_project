from setuptools import setup

setup(
    name='gemalto-project',
    version='0.4.0',
    description='Test for python developer position',
    url='https://github.com/codyc4321/gemalto_project',
    author='Cody Childers',
    author_email='cchilder@mail.usf.edu',
    license='MIT',
    packages=['gemalto_project'],
    install_requires=[],
    zip_safe=False,
)

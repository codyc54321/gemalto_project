#!/usr/bin/env python

""" pass in the input file number (from 1-5) you want to run """

from gemalto_project.bytes_to_data import parse_bytes_to_python_data_type

import sys

file_number = sys.argv[1]

parse_bytes_to_python_data_type(file_number)

#!/usr/bin/env python

import os
import sys
import time

""" utils for running the parser from bytes to normal python data for python developer evaluation """


def gather_bytes_array_from_file(input_file_number):
    path = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'inputs/input_'))
    with open(path + str(input_file_number), "rb") as f:
        bytes_array = []
        while True:
            b = f.read(1)
            if len(b) == 0:
                break
            # print("%s corresponds to %02x or in other words %s\n" % (b, ord(b), str(b.decode('utf-8'))), end='')
            bytes_array.append(ord(b))
        print("\n\nBytes array:")
        print(bytes_array)
        return bytes_array


def preparse_bytes(bytes_array):
    preparsed_array = []
    PARSING_NUM = False
    PARSING_NUM_COUNT = 0
    number = 0

    PARSING_STR = False
    string = ""

    for i, byte in enumerate(bytes_array):
        # Explains big endian at "big endian machine" halfway down:
        # https://betterexplained.com/articles/understanding-big-and-little-endian-byte-order/
        # each place is multiplied by 256, so the first spot (the biggest) is 3 to the left, so 256 * 256 * 256
        if PARSING_NUM and PARSING_NUM_COUNT < 4:
            endian_size = 3 - PARSING_NUM_COUNT
            if not endian_size:
                number += byte
            else:
                number += 256**endian_size * byte
            PARSING_NUM_COUNT += 1

        if PARSING_NUM and PARSING_NUM_COUNT == 4:
            PARSING_NUM = False
            PARSING_NUM_COUNT = 0
            preparsed_array.append(number)
            number = 0
            continue

        if byte == 1 and not PARSING_NUM and not PARSING_STR:
            PARSING_NUM = True

        if byte == 2 and not PARSING_NUM and not PARSING_STR:
            PARSING_STR = True
            continue

        if PARSING_STR:
            if  byte == 0:
                preparsed_array.append(string)
                PARSING_STR = False
                string = ""
                continue
            else:
                string += chr(byte)

        if not PARSING_NUM and not PARSING_STR:
            preparsed_array.append(byte)

    print("\n\nPreparsed array:")
    print(preparsed_array)
    return preparsed_array


def determine_blank_value(dict):
    values = dict.values()
    highest = 0
    for value in values:
        try:
            if value.startswith('xx_blank_'):
                this_value = int(value.strip('xx_blank_'))
                if this_value > highest:
                    highest = this_value
        except:
            pass
    if highest:
        next_order = highest + 1
        return 'xx_blank_' + str(next_order)
    else:
        return 'xx_blank_1'


def retrieve_blank_order_value(value):
    try:
        if value.startswith('xx_blank_'):
            return int(value.strip('xx_blank_'))
    except:
        pass
    return None


def preparse_dicts(preparsed_array):
    """
    [
        6, 4, 'first', 16777216, 'second', 'value for second', 'deep', 6, 3, 1, 'integer as key', 'mix',
        'it is possible to mix integers and strings', 2, 6, 1, 4, 19088743, 5, 25
    ]

    to

    [
        {'xxxkeyCountxxx': 4, 'first': 16777216, 'second': 'value for second', 'deep': 'xx_blank_1'},
        {'xxxkeyCountxxx': 3, 1: 'integer as key', 2: 'xx_blank_1',
        'mix': 'it is possible to mix integers and strings'}, {'xxxkeyCountxxx': 1, 4: 19088743}, 5, 25
    ]


    """
    # if there isn't a dict or list present, this isn't relevant
    if preparsed_array[0] not in [5, 6]:
        return preparsed_array

    dict_parsed_array = []
    current_dict = {}
    parsing_a_dict = False
    need_the_count = False
    key = None

    for i, item in enumerate(preparsed_array):
        if item == 6:
            if current_dict:
                dict_parsed_array.append(current_dict)
            parsing_a_dict = True
            need_the_count = True
            key = None
            current_dict = {}

            continue

        if item == 5:
            if i > 0:
                if key:
                    key = None
                if not preparsed_array[i - 1] in [1, 5, 6]:
                    parsing_a_dict = False
                    dict_parsed_array.append(current_dict)
                    current_dict = {}

        if parsing_a_dict:
            if need_the_count:
                # making an a key name very unlikely to collide with unknown keys
                current_dict['xxxkeyCountxxx'] = item
                need_the_count = False
                continue
            if not key:
                if preparsed_array[i + 1] in [5, 6]:
                    if len(current_dict.keys()) < (current_dict['xxxkeyCountxxx'] + 1):
                        current_dict[item] = determine_blank_value(current_dict)
                    else:
                        # else, the current dict has been filled with keys. This key must be for the previous dict,
                        # where one of the values is the dict you just filled
                        j = -1
                        while True:
                            dict_to_check = dict_parsed_array[j]
                            if len(dict_to_check.keys()) < (dict_to_check['xxxkeyCountxxx'] + 1):
                                dict_to_check[item] = determine_blank_value(dict_to_check)
                                break
                            else:
                                j -= 1
                else:
                    key = item
            else:
                current_dict[key] = item
                key = None

        if not parsing_a_dict:
            dict_parsed_array.append(item)

    if current_dict and current_dict not in dict_parsed_array:
        dict_parsed_array.append(current_dict)

    print("Dict parsed array:")
    print(dict_parsed_array)
    return dict_parsed_array


def remove_key_from_dicts(items, key_to_remove='xxxkeyCountxxx'):
    for item in items:
        if isinstance(item, dict):
            item.pop(key_to_remove, None)
            for value in item.values():
                if isinstance(value, list):
                    for thing in value:
                        if isinstance(thing, dict):
                            item.pop(key_to_remove, None)
                elif isinstance(value, dict):
                    value.pop(key_to_remove, None)
        elif isinstance(item, list):
            for thing in item:
                if isinstance(thing, dict):
                    thing.pop(key_to_remove, None)
    return items


def fill_lists(dict_parsed_array):
    list_parsed_array = []
    this_list = []
    parsing_list = False
    list_items = 0
    added_items = 0
    key_to_add = None

    for item in dict_parsed_array:
        # ints and strings have already been parsed, so this is fine.
        if key_to_add:
            for thing in dict_parsed_array:
                if isinstance(thing, dict):
                    if len(thing.keys()) < thing['xxxkeyCountxxx'] + 1:
                        thing[key_to_add] = item
                        continue
        if item == 5:
            for thing in dict_parsed_array:
                if isinstance(thing, dict):
                    # this means that item is short a key, and since we go left to right iterating on the list
                    # this first item should be the key
                    if len(thing.keys()) < thing['xxxkeyCountxxx'] + 1:
                        key_to_add = item
                        continue
            parsing_list = True
            continue

        if not parsing_list:
            list_parsed_array.append(item)
        else:
            # this should only run once, as soon as parsing_list is set to true
            if not list_items:
                assert type(item) is int
                list_items = item
                continue
            # this means the list has finished being created...add it and the current item to the parse array,
            # reset the params in case there's more than 1 list
            if list_items:
                if added_items == list_items:
                    parsing_list = False
                    list_items = 0
                    added_items = 0
                    assert this_list
                    list_parsed_array.append(this_list)
                    this_list = []
                    list_parsed_array.append(item)
                elif added_items < list_items:
                    this_list.append(item)
                else:
                    raise Exception("Added items should not exceed list items needed")

    # in case the loop ends on creating a list
    if this_list:
        list_parsed_array.append(this_list)

    remove_key_from_dicts(list_parsed_array)

    print("\n\nList parsed array:")
    print(list_parsed_array)
    return list_parsed_array


def fill_blank_items_into_dicts(list_parsed_array):
    for i, item in enumerate(list_parsed_array):
        if isinstance(item, dict):
            for key, value in item.items():
                blank_order = retrieve_blank_order_value(value)
                if blank_order:
                    # [i + blank_order] because if blank_order is 1, it's first so next in the array, 2 means 2 spots down, ect
                    item[key] = list_parsed_array[i + blank_order]
    return list_parsed_array[0]


def parse_bytes_to_python_data_type(input_file_number):
    bytes = gather_bytes_array_from_file(input_file_number)
    preparsed_bytes = preparse_bytes(bytes)
    preparsed_dicts = preparse_dicts(preparsed_bytes)
    parsed_lists = fill_lists(preparsed_dicts)
    final_result = fill_blank_items_into_dicts(parsed_lists)
    print("Final result:")
    print(final_result)
    return final_result
